var dataUrl = 'https://phl.carto.com/api/v2/sql?q=select%20lat,lon,address,media_url,requested_datetime,agency_responsible,status%20from%20public_cases_fc%20where%20requested_datetime%20%3E%20(CURRENT_DATE%20-%20INTERVAL%20%2730%20days%27)%20and%20agency_responsible%20=%20%27Office%20of%20Homeless%20Services%27&format=csv';
var maxZoom = 30;
var fieldSeparator = ',';
var baseUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
var baseAttribution = 'Data, imagery and map information provided by <a href="http://open.mapquest.co.uk" target="_blank">MapQuest</a>, <a href="http://www.openstreetmap.org/" target="_blank">OpenStreetMap</a> and contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/" target="_blank">CC-BY-SA</a>';
var subdomains = 'abc';
var clusterOptions = {showCoverageOnHover: true, maxClusterRadius: 50};
var labelColumn = "Name";
var opacity = 1.0;
